import webapp2
import markupsafe
import markdown
from webapp2_extras import jinja2

from google.appengine.api import users

def jinja2_factory(app):
    md = markdown.Markdown(safe_mode="escape")

    j = jinja2.Jinja2(app)
    j.environment.filters.update({
        'markdown': md.convert
        })
    return j

class BaseHandler(webapp2.RequestHandler):

    @webapp2.cached_property
    def user(self):
        return users.get_current_user()

    @webapp2.cached_property
    def jinja2(self):
        return jinja2.get_jinja2(
            factory=jinja2_factory,
            app=self.app)

    def render(self, template, **context):
        context.update({
            'url_for': self.url_for,
            'user': self.user,
            'login_url': users.create_login_url(self.request.uri),
            'logout_url': users.create_logout_url(self.request.uri),
            'uri': self.request.uri,
            })
        self.response.write(
            self.jinja2.render_template(
                template, 
                **context))
