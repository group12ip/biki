import webapp2
from webapp2 import Route
from webapp2_extras.routes import PathPrefixRoute, RedirectRoute

import common
import folders
import pages
import profiles
import pushover

import logging

class MainHandler(common.BaseHandler):
    def get(self):
    	fs = folders.Folder.query().fetch(100)
        self.render('index.html', folders=fs)

app = webapp2.WSGIApplication([
    Route('/', MainHandler, 'home'),

    RedirectRoute('/folders/', folders.FoldersHandler, 'folders', strict_slash=True),
    PathPrefixRoute('/folders', [
        Route('/create', folders.FolderCreateHandler, 'folder-create'),
        Route('/<id:[\w~.-]+>', folders.FolderHandler, 'folder'),
    ]),

    RedirectRoute('/profile', profiles.ProfileHandler, 'profile', strict_slash=True),

    Route('/tasks/pushover', pushover.PushOverWorker, 'tasks-pushover'),

    RedirectRoute('/<folder:[\w~.-]+>/', pages.PagesHandler, 'pages', strict_slash=True),
    PathPrefixRoute('/<folder:[\w~.-]+>', [
        Route('/create', pages.PageCreateHandler, 'page-create'),
        Route('/<id:\d+>', pages.PageHandler, 'page'),
        Route('/<id:\d+>/edit', pages.PageEditHandler, 'page-edit'),
        Route('/<id:\d+>/delete', pages.PageDeleteHandler, 'page-delete'),
    ]),
], debug=True)
