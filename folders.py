import webapp2
from webapp2_extras.appengine.users import admin_required

from google.appengine.api import users
from google.appengine.ext import ndb

import common

import re

class Folder(ndb.Model):
	title = ndb.StringProperty(required=True)
	creator = ndb.UserProperty(required=True)
	created = ndb.DateTimeProperty(auto_now_add=True)

class FoldersHandler(common.BaseHandler):

	@admin_required
	def get(self):
		folders = Folder.query().fetch(100)
		self.render('folders/index.html', folders=folders)

class FolderHandler(common.BaseHandler):

	@admin_required
	def get(self, id):
		folder = Folder.get_by_id(id)
		if folder:
			self.render('folders/folder.html', folder=folder)
		else:
			self.error(404)

class FolderCreateHandler(common.BaseHandler):

	@admin_required
	def get(self):
		self.render('folders/create.html')

	def post(self):
		if not users.is_current_user_admin():
			self.abort(401)

		title = self.request.get('title')
		short = self.request.get('short')

		if not (title and short):
			self.render('folders/create.html',
				title=title,
				short=short,
				error='Title and Short Title are required.')
			return

		if not re.match('[\w~.-]+$', short):
			self.render('folders/create.html',
				title=title,
				short=short,
				error="Short Title may only contain a-z, A-Z, 0-9, '~', '_', '.', or '-' characters.")
			return

		folder = Folder(
			key=ndb.Key(Folder, short),
			title=title,
			creator=self.user)
		folder.put()
		self.redirect('/folders/%s' % folder.key.id())

