import webapp2

from google.appengine.api import taskqueue
from google.appengine.api import urlfetch
from google.appengine.ext import ndb

import folders
import profiles

import urllib
import logging

PUSHOVER_API_TOKEN = 'vBqJJbVU1j95AqWdZODY2z17F5Fnd2'

class PushOverWorker(webapp2.RequestHandler):
	def post(self):
		folder = self.request.get('folder')
		title = self.request.get('title')
		message = self.request.get('message')
		url = self.request.get('url')

		key = ndb.Key(folders.Folder, folder)
		ps = profiles.Profile.query(profiles.Profile.pushover_subscriptions == key)
		for p in ps:
			if p.pushover_userkey:
				form = urllib.urlencode({
					'token': PUSHOVER_API_TOKEN,
					'user': p.pushover_userkey,
					'title': title + ' - Biki',
					'message': message,
					'url': url,
					})
				urlfetch.fetch('https://api.pushover.net/1/messages.json',
					payload=form, method=urlfetch.POST,
					headers={'Content-Type': 'application/x-www-form-urlencoded'})
