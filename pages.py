import webapp2
from webapp2_extras.appengine.users import login_required

from google.appengine.api import taskqueue
from google.appengine.api import urlfetch
from google.appengine.ext import ndb
from google.appengine.datastore.datastore_query import Cursor


import common
import folders

import urllib
import logging

PER_PAGE = 20

class Page(ndb.Model):
	title = ndb.StringProperty(required=True)
	content = ndb.TextProperty(required=True)
	creator = ndb.UserProperty(required=True)
	created = ndb.DateTimeProperty(auto_now_add=True)
	modified = ndb.DateTimeProperty(auto_now=True)
	folder = ndb.KeyProperty(kind=folders.Folder, required=True)

class PagesHandler(common.BaseHandler):

	def get(self, folder):
		f = folders.Folder.get_by_id(folder)
		if not f:
			self.abort(404)

		# http://moritzangermann.com/2009/simple-two-way-paging-solution-for-appengine/

		after  = self.request.get('after')
		before = self.request.get('before')

		qry = Page.query(Page.folder == f.key)
		if before:
			cursor = Cursor(urlsafe=before)
			revrs, prev, less = qry.order(-Page.created).fetch_page(
				PER_PAGE, start_cursor=cursor)
			pages, next, more = qry.order( Page.created).fetch_page(
				len(revrs), start_cursor=prev.reversed())
		elif after:
			cursor = Cursor(urlsafe=after)
			pages, next, more = qry.order( Page.created).fetch_page(
				PER_PAGE, start_cursor=cursor)
			revrs, prev, less = qry.order(-Page.created).fetch_page(
				len(pages), start_cursor=next.reversed())
		else:
			pages, next, more = qry.order( Page.created).fetch_page(
				PER_PAGE)
			less = False
			prev = None

		self.render('pages/index.html', 
			pages=pages, folder=f, 
			more=more, next=next,
			prev=prev, less=less)

class PageHandler(common.BaseHandler):

	def get(self, folder, id):
		f = folders.Folder.get_by_id(folder)
		if not f:
			self.abort(404)

		page = Page.get_by_id(int(id))
		if page:
			self.render('pages/page.html', page=page, folder=f)
		else:
			self.error(404)

class PageCreateHandler(common.BaseHandler):

	@login_required
	def get(self, folder):
		f = folders.Folder.get_by_id(folder)
		if not f:
			self.abort(404)
		
		self.render('pages/create.html', folder=f)

	def post(self, folder):
		if not self.user:
			self.abort(401)

		f = folders.Folder.get_by_id(folder)
		if not f:
			self.abort(404)

		title = self.request.get('title')
		content = self.request.get('content')

		if not (title and content):
			self.render('pages/create.html', 
				title=title,
				content=content,
				folder=f,
				error='Title and Content are required.')
			return

		page = Page(
			title=title,
			content=content,
			creator=self.user,
			folder=f.key)
		page.put()

		taskqueue.add(url='/tasks/pushover', params={
			'folder': folder,
			'title': 'Page %s created in %s' % (title, folder),
			'message': content[0:100],
			'url': self.url_for('page', folder=folder, id=page.key.id(), _full=True)
			})

		# form = urllib.urlencode({
		# 			'token': 'vBqJJbVU1j95AqWdZODY2z17F5Fnd2',
		# 			'user': '2SFe1HVxKv39TlKdgJKkfKaVLh96mX',
		# 			'title': 'Biki',
		# 			'message': 'Page created',
		# 			})
		# urlfetch.fetch('https://api.pushover.net/1/messages.json',
		# 	payload=form, method=urlfetch.POST,
		# 	headers={'Content-Type': 'application/x-www-form-urlencoded'})
		self.redirect('/%s/%d' % (folder, page.key.id()))



class PageEditHandler(common.BaseHandler):

	@login_required
	def get(self, folder, id):
		f = folders.Folder.get_by_id(folder)
		if not f:
			self.abort(404)

		page = Page.get_by_id(int(id))
		if not page:
			self.abort(404)

		self.render('pages/edit.html', page=page, folder=f)

	def post(self, folder, id):
		if not self.user:
			self.abort(401)

		f = folders.Folder.get_by_id(folder)
		if not f:
			self.abort(404)

		page = Page.get_by_id(int(id))
		if not page:
			self.abort(404)

		content = self.request.get('content')
		page.content = content
		page.put()
		self.redirect('/%s/%d' % (folder, page.key.id()))

class PageDeleteHandler(common.BaseHandler):

	@login_required
	def get(self, folder, id):
		f = folders.Folder.get_by_id(folder)
		if not f:
			self.abort(404)

		page = Page.get_by_id(int(id))
		if not page:
			self.abort(404)

		self.render('pages/delete.html', page=page, folder=f)

	def post(self, folder, id):
		if not self.user:
			self.abort(401)

		f = folders.Folder.get_by_id(folder)
		if not f:
			self.abort(404)

		page = Page.get_by_id(int(id))
		if not page:
			self.abort(404)

		page.key.delete()
		self.redirect('/%s/' % folder)


