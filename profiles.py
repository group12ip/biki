import webapp2
from webapp2_extras.appengine.users import login_required

from google.appengine.ext import ndb

import common
import folders

class Profile(ndb.Model):
	user = ndb.UserProperty(required=True)
	pushover_userkey = ndb.StringProperty()
	#pushover_device = ndb.StringProperty()
	pushover_subscriptions = ndb.KeyProperty(
		kind=folders.Folder, repeated=True)

class ProfileHandler(common.BaseHandler):

	@login_required
	def get(self):
		p = Profile.get_or_insert(
			self.user.email(),
			user=self.user)
		fs = folders.Folder.query().fetch(100)
		self.render('profile.html', profile=p, folders=fs)

	def post(self):
		if not self.user:
			self.abort(401)

		p = Profile.get_by_id(self.user.email())
		if not p:
			self.abort(404)

		p.pushover_userkey = self.request.get('pushover_userkey')
		#p.pushover_device = self.request.get('pushover_device')
		subs = self.request.get_all('pushover_subscriptions')
		keys = [ndb.Key(folders.Folder, sub) for sub in subs]
		p.pushover_subscriptions = keys

		p.put()

		self.redirect('/profile')
